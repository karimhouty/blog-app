<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Profil;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
           'name' => 'Admin',
           'email' => 'admin@mail.com',
           'password' => bcrypt('admin'),
            'admin' => 1
        ]);

        Profil::create([
           'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.png',
            'about' => 'A software engineer is a person who applies the principles of software engineering to the design, development, maintenance, testing, and evaluation of computer software.',
            'facebook' => 'facebook.com',
            'youtube' => 'youtube.com'
        ]);
    }
}
