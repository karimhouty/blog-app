<?php

use Illuminate\Database\Seeder;
use App\Setting;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'site_name' => "Laravel's Blog",
            'contact_number' => '+212612345678',
            'contact_email' => 'admin@mail.com',
            'address' => 'Morocco, Settat'
        ]);
    }
}
