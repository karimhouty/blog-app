<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use App\Setting;

class FrontEndController extends Controller
{
    public function index(){
        $settings = Setting::first();
        $categories = Category::take(5)->get();
        $first_post = Post::orderBy('created_at', 'desc')->first();
        $second_post = Post::orderBy('created_at', 'desc')->skip(1)->take(1)->get()->first();
        $third_post = Post::orderBy('created_at', 'desc')->skip(2)->take(1)->get()->first();
        $laravel = Category::find(1);
        $wordpress = Category::find(2);
        return view('welcome',
            compact('settings', 'categories', 'first_post', 'second_post', 'third_post', 'laravel', 'wordpress'));
    }
}
