<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\User;
use App\Profil;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt('root')
        ]);

        $profil = Profil::create([
            'user_id' => $user->id,
            'avatar' => 'uploads/avatars/1.png'
        ]);

        Toastr::success('User Created');
        return redirect()->route('users');
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->profil->delete();
        $user->delete();

        Toastr::success('User Deleted');
        return redirect()->back();

    }

    public function admin($id){
        $user = User::findOrFail($id);
        $user->admin = 1;
        $user->save();
        Toastr::success('Permission changed');
        return redirect()->back();
    }

    public function not_admin($id){
        $user = User::findOrFail($id);
        $user->admin = 0;
        $user->save();
        Toastr::success('Permission changed');
        return redirect()->back();
    }
}
