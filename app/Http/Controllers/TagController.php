<?php

namespace App\Http\Controllers;

use App\Tag;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        $tags = Tag::all();
        return view('admin.tags.index', compact('tags'));
    }

    public function create()
    {
        return view('admin.tags.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        Tag::create([
            'tag' => $request->tag
        ]);

        Toastr::success('Tag Created');
        return redirect()->route('tags');
    }

    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('admin.tags.edit', compact('tag'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tag' => 'required'
        ]);

        $tag = Tag::findOrFail($id);
        $tag->tag = $request->tag;

        $tag->save();

        Toastr::success('Tag Updated');
        return redirect()->route('tags');
    }

    public function destroy($id)
    {
        Tag::destroy($id);
        Toastr::success('Tag Deleted');
        return redirect()->back();
    }
}
