<?php

namespace App\Http\Controllers;

use App\Tag;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Category;
use App\Post;

class PostController extends Controller
{

    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        if($categories->count() == 0 || $tags->count() == 0){
            Toastr::info('You must have some categories & tags before attempting create a new post');
            return redirect()->back();
        }
        return view('admin.posts.create', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
           'title' => 'required',
           'featured' => 'required|image',
           'content' => 'required',
           'category_id' => 'required',
            'tags' => 'required'
        ]);

        $featured = $request->featured;
        $featured_new_name = time().'-'.$featured->getClientOriginalName();
        $featured->move('uploads/posts', $featured_new_name);

        $post = Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'featured' => 'uploads/posts/'.$featured_new_name,
            'category_id' => $request->category_id,
            'slug' => str_slug($request->title)
        ]);

        $post->tags()->attach($request->tags);

        Toastr::success('Post Created');

        return redirect()->route('posts');
    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.posts.edit', compact('post', 'categories', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
           'title' => 'required',
           'content' => 'required',
           'category_id' => 'required'
        ]);

        $post = Post::findOrFail($id);

        if($request->hasFile('featured')){
            $featured = $request->featured;

            $featured_new_name = time().'-'.$featured->getClientOriginalName();

            $featured->move('uploads/posts/', $featured_new_name);

            $post->featured = 'uploads/posts/'.$featured_new_name;
        }

        $post->title = $request->title;
        $post->content = $request->content;
        $post->category_id = $request->category_id;

        $post->save();

        $post->tags()->sync($request->tags);

        Toastr::success('Post Updated');
        return redirect()->route('posts');
    }

    public function trash($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        Toastr::success('Post Trashed, Go To Post Trashed Section to restore it');
        return redirect()->back();
    }

    public function trashed(){
        $posts = Post::onlyTrashed()->get();
        return view('admin.posts.trashed', compact('posts'));
    }

    public function destroy($id){
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->forceDelete();
        Toastr::success('Post Deleted Permanently');
        return redirect()->back();
    }

    public function restore($id){
        $post = Post::withTrashed()->where('id', $id)->first();
        $post->restore();
        Toastr::success('Post Restored');
        return redirect()->route('posts');
    }
}
