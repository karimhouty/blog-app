<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('admin.categories.index', compact('categories'));

    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();
        Toastr::success('Category Created');
        return redirect()->route('categories');

    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.categories.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();
        Toastr::success('Category Updated');
        return redirect()->route('categories');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        foreach ($category->posts as $post){
            $post->forceDelete();
        }
        $category->delete();
        Toastr::success('Category Deleted');
        return redirect()->route('categories');
    }
}
