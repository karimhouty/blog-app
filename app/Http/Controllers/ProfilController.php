<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Brian2694\Toastr\Facades\Toastr;

class ProfilController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        return view('admin.users.profil', compact('user'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'facebook' => 'required|url',
            'youtube' => 'required|url',
        ]);

        $user = Auth::user();

        if($request->hasFile('avatar')){
            $avatar = $request->avatar;
            $avatar_new_name = time().'-'.$avatar->getClientOriginalName();
            $avatar->move('uploads/avatars/', $avatar_new_name);
            $user->profil->avatar = 'uploads/avatars/'.$avatar_new_name;
            $user->profil->save();
        }

        $user->name = $request->name;
        $user->email = $request->email;

        $user->profil->facebook = $request->facebook;
        $user->profil->youtube = $request->youtube;

        $user->save();
        $user->profil->save();

        if($request->has('password')){
            $user->password = bcrypt($request->password);
            $user->save();
        }

        Toastr::success('Profile Updated');
        return redirect()->route('home');
    }

}
