<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Brian2694\Toastr\Facades\Toastr;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()->admin){
            Toastr::success('Permission Denied');
            return redirect()->back();
        }
        return $next($request);
    }
}
