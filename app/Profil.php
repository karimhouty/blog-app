<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getAvatarAttribute($avatar){
        return asset($avatar);
    }

    protected $fillable = [
        'avatar', 'user_id', 'facebook', 'youtube', 'about'
    ];
}
