@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Edit Your Site Settings</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('setting.update') }}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Site Name : </label>
                    <input type="text" name="site_name" class="form-control" value="{{$settings->site_name}}">
                </div>

                <div class="form-group">
                    <label>Address : </label>
                    <input type="text" name="address" class="form-control" value="{{$settings->address}}">
                </div>

                <div class="form-group">
                    <label>Contact Number : </label>
                    <input type="text" name="contact_number" class="form-control" value="{{$settings->contact_number}}">
                </div>

                <div class="form-group">
                    <label>Contact Email : </label>
                    <input type="text" name="contact_email" class="form-control" value="{{$settings->contact_email}}">
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Settings</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

@stop