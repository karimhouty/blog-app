@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Tags List</div>
        <div class="card-body">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Tag</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @if($tags->count() > 0)
                        @foreach($tags as $tag)
                            <tr>
                                <td> {{ $tag->id }} </td>
                                <td> {{ $tag->tag  }}</td>
                                <td>
                                    <a href="{{ route('tag.edit', ['id' => $tag->id]) }}" class="btn btn-sm btn-success">Edit</a>
                                    <a href="{{ route('tag.delete', ['id' => $tag->id]) }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th colspan="3" class="text-center text-danger">
                                No Tags
                            </th>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
@stop