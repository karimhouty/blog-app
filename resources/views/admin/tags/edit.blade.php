@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Edit Tag</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('tag.update', ['id' => $tag->id]) }}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Tag : </label>
                    <input type="text" name="tag" class="form-control" value="{{$tag->tag}}">
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Tag</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



@stop
