@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Update Your Profile</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('user.profil.update')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Name : </label>
                    <input type="text" name="name" class="form-control" value="{{$user->name}}">
                </div>

                <div class="form-group">
                    <label>Email : </label>
                    <input type="email" name="email" class="form-control" value="{{$user->email}}">
                </div>

                <div class="form-group">
                    <label>New Password :</label>
                    <input type="password" name="password" class="form-control">
                </div>

                <div class="form-group">
                    <label>Avatar : </label>
                    <input type="file" name="avatar" class="form-control">
                </div>

                <div class="form-group">
                    <label>About : </label>
                    <textarea name="content" id="content" cols="30" rows="5" class="form-control">
                        {{$user->profil->about}}
                    </textarea>
                </div>

                <div class="form-group">
                    <label>Facebook Profile : </label>
                    <input type="text" name="facebook" class="form-control" value="{{$user->profil->facebook}}">
                </div>

                <div class="form-group">
                    <label>Youtube Account : </label>
                    <input type="text" name="youtube" class="form-control" value="{{$user->profil->youtube}}">
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Profile</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop