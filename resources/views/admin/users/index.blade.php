@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Users List</div>
        <div class="card-body">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Permissions</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @if($users->count() > 0)
                        @foreach($users as $user)
                            <tr>
                                <td> <img src="{{ $user->profil->avatar }}" alt="" class="rounded" width="60px" height="60px"> </td>
                                <td> {{ $user->name  }}</td>
                                <td>
                                    @if($user->admin)
                                        <a href="{{route('user.not_admin', ['id' => $user->id])}}" class="btn btn-danger btn-sm">Remove Permission</a>
                                    @else
                                        <a href="{{route('user.admin', ['id' => $user->id])}}" class="btn btn-info btn-sm">Make Admin</a>
                                    @endif
                                </td>
                                <td>
                                    @if(Auth::id() !== $user->id )
                                        <a href="{{route('user.delete', ['id' => $user->id])}}" class="btn btn-danger btn-sm">Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th colspan="3" class="text-center text-danger">
                                No Posts
                            </th>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
@stop