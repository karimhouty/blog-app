@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Create new User</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('user.store') }}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Name : </label>
                    <input type="text" name="name" class="form-control">
                </div>

                <div class="form-group">
                    <label>Email : </label>
                    <input type="email" name="email" class="form-control">
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Store User</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop