@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Categories List</div>
        <div class="card-body">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @if($categories->count() > 0)
                        @foreach($categories as $category)
                            <tr>
                                <td> {{ $category->id }} </td>
                                <td> {{ $category->name  }}</td>
                                <td>
                                    <a href="{{ route('category.edit', ['id' => $category->id]) }}" class="btn btn-sm btn-success">Edit</a>
                                    <a href="{{ route('category.delete', ['id' => $category->id]) }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th colspan="3" class="text-center text-danger">
                                No Categories
                            </th>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
@stop