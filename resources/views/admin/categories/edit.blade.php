@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Create new Category</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('category.update', ['id' => $category->id]) }}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Name : </label>
                    <input type="text" name="name" class="form-control" value="{{$category->name}}">
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Update Category</button>
                    </div>
                </div>
            </form>
        </div>
    </div>



@stop
