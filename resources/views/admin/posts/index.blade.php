@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Posts List</div>
        <div class="card-body">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Title</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    @if($posts->count() > 0)
                        @foreach($posts as $post)
                            <tr>
                                <td> <img src="{{ $post->featured }}" alt="" class="rounded" width="100" height="50"> </td>
                                <td> {{ $post->title  }}</td>
                                <td>
                                    <a href="{{ route('post.edit', ['id' => $post->id]) }}" class="btn btn-sm btn-success">Edit</a>
                                    <a href="{{ route('post.trash', ['id' => $post->id]) }}" class="btn btn-sm btn-danger">Trash</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <th colspan="3" class="text-center text-danger">
                                No Posts
                            </th>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>
    </div>
@stop
