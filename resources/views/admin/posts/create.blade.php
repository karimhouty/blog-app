@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">Create new Post</div>
        <div class="card-body">

            @include('admin.errors.error')

            <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Title : </label>
                    <input type="text" name="title" class="form-control">
                </div>

                <div class="form-group">
                    <label>Featured Image : </label>
                    <input type="file" name="featured" class="form-control">
                </div>

                <div class="form-group">
                    <label for="category">Gategory : </label>
                    <select name="category_id" id="category" class="form-control">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>Tags : </label><br>
                    @foreach($tags as $tag)
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" name="tags[]" value="{{$tag->id}}">
                            <label class="form-check-label" for="inlineCheckbox1">{{$tag->tag}}</label>
                        </div>
                    @endforeach
                </div>

                <div class="form-group">
                    <label>Content : </label>
                    <textarea name="content" id="content" cols="30" rows="5" class="form-control"></textarea>
                </div>

                <div class="form-group">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">Store Post</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop