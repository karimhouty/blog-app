"# blog-app"

Quick Start After Cloning The App

1. Install Dependencies : `composer install`

2. Create a db called `blog`

3. Run Migrations : `php artisan migrate`

4. Import Admin Account & Settings : `php artisan db:seed`

5. If you get an error about an encryption key : `php artisan key:generate`
