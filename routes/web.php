<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function (){
    $cat = App\Category::find(1);
    return $cat->posts()->take(2)->get();
});

Route::get('/', 'FrontEndController@index')->name('welcome');

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/posts', 'PostController@index')->name('posts');
    Route::get('/posts/trashed', 'PostController@trashed')->name('posts.trashed');
    Route::get('/post/create', 'PostController@create')->name('post.create');
    Route::post('/post/store', 'PostController@store')->name('post.store');
    Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
    Route::post('/post/update/{id}', 'PostController@update')->name('post.update');
    Route::get('/post/trash/{id}', 'PostController@trash')->name('post.trash');
    Route::get('/post/restore/{id}', 'PostController@restore')->name('post.restore');
    Route::get('/post/delete/{id}', 'PostController@destroy')->name('post.delete');

    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/category/create', 'CategoryController@create')->name('category.create');
    Route::post('/category/store', 'CategoryController@store')->name('category.store');
    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
    Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');
    Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');

    Route::get('/tags', 'TagController@index')->name('tags');
    Route::get('/tag/create', 'TagController@create')->name('tag.create');
    Route::post('/tag/store', 'TagController@store')->name('tag.store');
    Route::get('/tag/edit/{id}', 'TagController@edit')->name('tag.edit');
    Route::post('/tag/update/{id}', 'TagController@update')->name('tag.update');
    Route::get('/tag/delete/{id}', 'TagController@destroy')->name('tag.delete');

    Route::get('/users', 'UserController@index')->name('users');
    Route::get('/user/create', 'UserController@create')->name('user.create');
    Route::post('/user/store', 'UserController@store')->name('user.store');
    Route::get('/user/delete/{id}', 'UserController@destroy')->name('user.delete');
    Route::get('/user/admin/{id}', 'UserController@admin')->name('user.admin');
    Route::get('/user/not_admin/{id}', 'UserController@not_admin')->name('user.not_admin');
    Route::get('/user/profil', 'ProfilController@index')->name('user.profil');
    Route::post('/user/profil/update', 'ProfilController@update')->name('user.profil.update');

    Route::get('/settings', 'SettingController@index')->name('settings');
    Route::post('/settings/update', 'SettingController@update')->name('setting.update');

});


